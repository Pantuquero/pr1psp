package org.pantuquero.gestordescargas.threads;

import org.pantuquero.gestordescargas.gui.PanelDescarga;

import javax.swing.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Pantuquero on 11/12/2014.
 */
public class Momento extends SwingWorker {
    PanelDescarga panel;
    private int hora;
    private int minuto;

    public Momento(PanelDescarga panel, int hora, int minuto){
        this.panel = panel;
        this.hora = hora;
        this.minuto = minuto;
    }

    @Override
    protected Object doInBackground() throws Exception {

        while (true){
            GregorianCalendar calendario = new GregorianCalendar();
            if(hora == calendario.get(Calendar.HOUR_OF_DAY) && minuto == calendario.get(Calendar.MINUTE)){
                panel.descargar();
                break;
            }else{
                Thread.sleep(1000);
            }
        }

        return null;
    }
}
