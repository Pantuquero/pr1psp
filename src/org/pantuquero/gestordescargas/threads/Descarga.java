package org.pantuquero.gestordescargas.threads;

import org.pantuquero.gestordescargas.gui.Ventana;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Pantuquero on 10/12/2014.
 */
public class Descarga extends SwingWorker<Void, Integer> {
    private URL url;
    private String ruta;
    private JLabel lbEstado;
    private boolean pausa;
    private boolean salir;

    public Descarga(String url, String ruta, JLabel etiqueta) throws MalformedURLException, FileNotFoundException {
        this.url = new URL(url);
        this.ruta = ruta;
        this.lbEstado = etiqueta;
        this.pausa = false;
        this.salir = false;
    }

    @Override
    protected Void doInBackground() throws Exception {
        lbEstado.setText("Iniciando descarga...");
        URLConnection conexion = url.openConnection();
        int tamano = conexion.getContentLength();

        InputStream inputStream = url.openStream();
        FileOutputStream fos = new FileOutputStream(this.ruta);
        byte[] bytes = new byte[2048];
        int longitud = 0;
        int progresoDescarga = 0;

        while ((longitud = inputStream.read(bytes)) != -1 && !salir) {

            fos.write(bytes, 0, longitud);

            progresoDescarga += longitud;
            lbEstado.setText("Descargado " + progresoDescarga + " de " + tamano + " Bytes - " + String.valueOf((progresoDescarga * 100 / tamano)) + "%");

            while(this.pausa == true){
                System.out.println("pausa");
                lbEstado.setText("Descaga en pausa");
                Thread.sleep(1000);
            }
        }

        inputStream.close();
        fos.close();

        if(salir){
            lbEstado.setText("Descarga cancelada");
            File archivo = new File(ruta);
            archivo.delete();
        }else{
            lbEstado.setText("Descarga completada");
            Ventana.log.info("Descarga completada");
        }

        setProgress(100);

        return null;
    }

    public void setPausa(boolean pausa) {
        this.pausa = pausa;
    }

    public void setSalir(boolean salir) {
        this.salir = salir;
    }
}
