package org.pantuquero.gestordescargas.gui;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Pantuquero on 11/12/2014.
 */
public class VerLog {
    public JPanel panelVerLog;
    private JScrollPane spVerLog;
    private JTextArea taVerLog;

    public VerLog(){
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("Descargas.log"));
            String linea = bufferedReader.readLine();
            while (linea != null){
                taVerLog.setLineWrap(true);
                taVerLog.append(linea + "\n");
                linea = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
