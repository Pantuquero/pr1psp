package org.pantuquero.gestordescargas.gui;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Pantuquero on 10/12/2014.
 */
public class Ventana {
    private JPanel panelVentana;
    private JTextField tfRuta;
    private JButton btExaminar;
    private JButton btNuevaDescarga;
    private JPanel panelDescargas;
    private JScrollPane scrollDescargas;

    private String rutaDescarga;
    public static int numeroDescargas = 0;
    public static ExecutorService piscinaHilos;

    public static Logger log;

    public Ventana(final JMenuItem importarEnlaces, final JMenuItem numDescSin){
        this.numeroDescargas = 0;

        try {
            log = Logger.getLogger(Ventana.class);
            PatternLayout pattern = new PatternLayout("%d %-5p [%t]: %m%n");
            this.log.addAppender(new FileAppender(pattern, "Descargas.log", true));
        } catch (IOException e) {
            System.out.println("Error al cargar el log");
        }
        log.info("--- NUEVA SESION ---");

        btNuevaDescarga.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaDescarga();
            }
        });

        btExaminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                examinar();
                if(!tfRuta.getText().equals("")){
                    btNuevaDescarga.setEnabled(true);
                    importarEnlaces.setEnabled(true);
                }
            }
        });

        importarEnlaces.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarArchivoEnlaces();
            }
        });

        numDescSin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ventanaPreferencias();
            }
        });

    }

    private void ventanaPreferencias(){
        JFrame framePreferencias = new JFrame("Preferencias");
        framePreferencias.setContentPane(new Preferencias().panelPreferencias);
        framePreferencias.pack();
        framePreferencias.setVisible(true);
    }

    private void nuevaDescarga(){
        PanelDescarga panelDescarga = new PanelDescarga(this.rutaDescarga);
        panelDescargas.add(panelDescarga.getPanel());
        panelDescargas.updateUI();
        log.info("Panel de descarga añadido");
    }

    private void examinar(){
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        jFileChooser.setApproveButtonText("Seleccionar carpeta");

        if(jFileChooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION){
            return;
        }

        this.rutaDescarga = jFileChooser.getSelectedFile().getAbsolutePath() + "\\";
        tfRuta.setText(this.rutaDescarga);
        log.info("Ruta de descarga establecida en " + this.rutaDescarga);
    }

    private void cargarArchivoEnlaces(){
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jFileChooser.setApproveButtonText("Seleccionar");

        if(jFileChooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION){
            return;
        }

        String ruta = jFileChooser.getSelectedFile().getAbsolutePath();

        Vector<String> cadenas = new Vector<String>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(ruta));
            String linea = bufferedReader.readLine();
            while (linea != null){
                cadenas.add(linea);
                linea = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(String url : cadenas){
            PanelDescarga panelDescarga = new PanelDescarga(this.rutaDescarga);
            panelDescarga.tfURL.setText(url);
            panelDescargas.add(panelDescarga.getPanel());
            panelDescargas.updateUI();
            log.info("Panel de descarga añadido mediante fichero de descargas");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Administrador de descargas");

        JMenuBar menuBar = new JMenuBar();

        JMenu archivo = new JMenu("Archivo");
            JMenuItem archivoImportarEnlaces = new JMenuItem("Importar enlaces");
            archivoImportarEnlaces.setEnabled(false);
        archivo.add(archivoImportarEnlaces);
        menuBar.add(archivo);

        JMenu ver = new JMenu("Ver");
            JMenuItem verLog = new JMenuItem("log");
        ver.add(verLog);
        menuBar.add(ver);

        JMenu preferencias = new JMenu("Preferencias");
            JMenuItem numDescSim = new JMenuItem("Numero de descargas simultaneas");
        preferencias.add(numDescSim);
        menuBar.add(preferencias);

        frame.setJMenuBar(menuBar);

        verLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                log.info("Log consultado");
                JFrame frameLog = new JFrame("Log");
                frameLog.setContentPane(new VerLog().panelVerLog);
                frameLog.pack();
                frameLog.setVisible(true);
            }
        });

        frame.setContentPane(new Ventana(archivoImportarEnlaces, numDescSim).panelVentana);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}