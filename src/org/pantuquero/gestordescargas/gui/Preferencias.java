package org.pantuquero.gestordescargas.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;

/**
 * Created by Pantuquero on 12/12/2014.
 */
public class Preferencias {
    public JPanel panelPreferencias;
    private JTextField tfNumeroDescargasSimultaneas;
    private JButton btAceptar;

    public Preferencias(){
        tfNumeroDescargasSimultaneas.setText(String.valueOf(Ventana.numeroDescargas));

        btAceptar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDescargasSimultaneas();
                JOptionPane.showMessageDialog(null, "Limite de descargas establecido", "Información", JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }

    private void guardarDescargasSimultaneas(){
        if(!tfNumeroDescargasSimultaneas.getText().equals("")){
            int numero = 0;

            try{
                numero = Integer.parseInt(tfNumeroDescargasSimultaneas.getText());
            }catch (NumberFormatException e){
                Ventana.log.error("Numero de hilos introducido incorrecto");
            }

            if(numero>0){
                Ventana.piscinaHilos = Executors.newFixedThreadPool(numero);
                Ventana.log.info("Limite de descargas simultaneas establecido en" + numero);
            }else if(numero==0 && Ventana.piscinaHilos != null){
                Ventana.piscinaHilos.shutdownNow();
            }
            Ventana.numeroDescargas = numero;
        }
    }
}
