package org.pantuquero.gestordescargas.gui;

import org.pantuquero.gestordescargas.threads.Descarga;
import org.pantuquero.gestordescargas.threads.Momento;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Pantuquero on 10/12/2014.
 */
public class PanelDescarga {
    private JPanel panel;
    private JLabel lbEstado;
    private JButton btDescargar;
    private JButton btCancelar;
    public JTextField tfURL;
    private JLabel lbURL;
    private JLabel lbRutaDescarga;
    private JTextField tfHora;
    private JTextField tfMinutos;
    private JLabel lbHora;
    private JLabel lbMinutos;
    private JButton btProgramar;

    private String rutaDescarga;
    private String nombreArchivo;
    private Descarga descarga = null;

    public PanelDescarga(String ruta){
        this.rutaDescarga = ruta;
        lbRutaDescarga.setText(this.rutaDescarga);

        btDescargar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!tfURL.getText().equals("")){
                    String dividir[] = tfURL.getText().split("/");
                    nombreArchivo = dividir[dividir.length-1];
                    File archivo = new File(rutaDescarga + nombreArchivo);
                    if(!archivo.exists()){
                        descargar();
                    }else{
                        JOptionPane.showMessageDialog(null, "El archivo que esta intentando guardar ya existe, por favor escoja otra ruta o elimine el archivo", "El archivo ya existe", JOptionPane.WARNING_MESSAGE);
                        Ventana.log.error("El archivo que se intentaba descargar ya existe");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Introduzca una URL", "URL incorrecta", JOptionPane.WARNING_MESSAGE);
                    Ventana.log.error("Introducida URL vacía");
                }
            }
        });


        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(btCancelar.getText().equals("Cancelar")){
                    descarga.setPausa(true);
                    int opcion = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el archivo que estaba descargando?");
                    if(opcion == JOptionPane.YES_OPTION){
                        descarga.setPausa(false);
                        descarga.setSalir(true);
                        Ventana.log.info("Descarga cancelada y archivo eliminado");
                    }else if(opcion == JOptionPane.NO_OPTION){
                        btCancelar.setText("Reanudar");
                        Ventana.log.info("Descarga pausada");
                    }else if(opcion == JOptionPane.CANCEL_OPTION){
                        descarga.setPausa(false);
                    }
                }else if (btCancelar.getText().equals("Reanudar")){
                    descarga.setPausa(false);
                    btCancelar.setText("Cancelar");
                    Ventana.log.info("Descarga reanudada");
                }
            }
        });


        btProgramar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!tfURL.getText().equals("") && !tfHora.getText().equals("") && !tfMinutos.getText().equals("")){
                    String dividir[] = tfURL.getText().split("/");
                    nombreArchivo = dividir[dividir.length-1];
                    File archivo = new File(rutaDescarga + nombreArchivo);
                    if(!archivo.exists()){
                        Momento momento = new Momento(PanelDescarga.this, Integer.parseInt(tfHora.getText()), Integer.parseInt(tfMinutos.getText()));
                        momento.execute();
                        tfHora.setEnabled(false);
                        tfMinutos.setEnabled(false);
                        btProgramar.setEnabled(false);
                        btDescargar.setEnabled(false);
                        lbEstado.setText("Programada para las " + tfHora.getText() + " horas y " + tfMinutos.getText() + " minutos");
                        Ventana.log.info("Descarga del archivo " + nombreArchivo + " programada para las " + tfHora.getText() + " horas y " + tfMinutos.getText() + " minutos");
                    }else{
                        JOptionPane.showMessageDialog(null, "El archivo que esta intentando guardar ya existe, por favor escoja otra ruta o elimine el archivo", "El archivo ya existe", JOptionPane.WARNING_MESSAGE);
                        Ventana.log.error("El archivo que se intentaba descargar ya existe");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Rellene todos los campos", "Campo vacio", JOptionPane.WARNING_MESSAGE);
                    Ventana.log.error("Algun campo vacio al programar la hora");
                }
            }
        });
    }

    public void descargar(){
        try {
            URL url =  new URL(tfURL.getText());
            descarga = new Descarga(tfURL.getText(), rutaDescarga + nombreArchivo, this.lbEstado);
            descarga.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent event) {
                    if (event.getNewValue().equals(100)) {
                        btDescargar.setEnabled(true);
                        btCancelar.setEnabled(false);
                        btProgramar.setEnabled(true);
                        tfHora.setEnabled(true);
                        tfMinutos.setEnabled(true);
                    }
                }
            });
            btDescargar.setEnabled(false);
            btCancelar.setEnabled(true);
            btProgramar.setEnabled(false);
            tfHora.setEnabled(false);
            tfMinutos.setEnabled(false);

            Ventana.log.info("Descarga del archivo " + nombreArchivo +  " iniciada");
            if(Ventana.numeroDescargas != 0){
                Ventana.piscinaHilos.submit(descarga);
                Ventana.piscinaHilos.execute(descarga);
            }else{
                descarga.execute();
            }
        } catch (Exception e) {
            if (e instanceof MalformedURLException){
                JOptionPane.showMessageDialog(null, "URL incorrecta", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
                Ventana.log.error("Introducida URL incorrecta");
            }
            else if (e instanceof FileNotFoundException){
                JOptionPane.showMessageDialog(null, "No se ha podido leer el fichero origen", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
                Ventana.log.error("Fichero de origen incorrecto");
            }
            else{
                JOptionPane.showMessageDialog(null, "No se ha podido leer el fichero origen", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
                Ventana.log.error("Fichero de origen incorrecto");
            }
            tfHora.setEnabled(true);
            tfMinutos.setEnabled(true);
            btProgramar.setEnabled(true);
            btCancelar.setEnabled(false);
            btDescargar.setEnabled(true);

            //e.printStackTrace();
        }
    }

    public JPanel getPanel() {
        return panel;
    }
}
